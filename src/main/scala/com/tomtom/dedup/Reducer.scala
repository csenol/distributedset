package com.tomtom.dedup

import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.PoisonPill
import akka.actor.Stash
import java.io.PrintWriter
import java.io.Writer
import scala.concurrent.duration._

trait Reducer extends Actor {
  import context.dispatcher
  val writer: Writer

  private var responseQueue = List.empty[Response]
  def receiveRespone(waitingForLine: Int, lastLine: Int = -1): Receive = {
    case Response(lineNumber, line, isDuplicate) if lineNumber == waitingForLine => {
      if (!isDuplicate) {
        writer.write(s"${line}\n")
      }
      if (lineNumber == lastLine) {
        writer.close()
	self ! PoisonPill
      }
      context.become(receiveRespone(waitingForLine + 1))
    }
    //Warn thread for a delayed line response. Skipping this line
    case k @ DontWaitForLine(lineNumber) => if (lineNumber == waitingForLine) {
      responseQueue.foreach(self ! _)
      context.become(receiveRespone(waitingForLine + 1))
    }
    //If we received a response with further line number
    case m @ Response(lineNumber, _, _) => if (lineNumber > waitingForLine) {
      responseQueue = responseQueue :+ m
      //send only one-time warning to skip long-time-waited line
      context.system.scheduler.scheduleOnce(100 millis, self, DontWaitForLine(waitingForLine))
    }
    // reader reached en of file. We are setting it here
    case EndOfFile(n) => if (waitingForLine > n){
      writer.close
      self ! PoisonPill
    }
    
    else
      context.become(receiveRespone(waitingForLine, n))
  }

  def receive = receiveRespone(1)
}

class ReducerImp(path: String) extends Reducer {
  override val writer = new PrintWriter(path, "UTF-8")
}

