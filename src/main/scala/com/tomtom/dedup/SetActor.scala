package com.tomtom.dedup

import akka.actor.Actor
import akka.actor.ActorRef

class SetActor extends Actor {

  def receiveRequest(set: Set[String]):Receive = {
    case Request(lineNumber, line) => 
      if (set(line)) {
        sender ! Response(lineNumber, line, duplicate = true)
      } else {
	sender ! Response(lineNumber, line, duplicate = false)
	context.become(receiveRequest(set + line))
      }
    }
  def receive = receiveRequest(Set.empty[String])
}
