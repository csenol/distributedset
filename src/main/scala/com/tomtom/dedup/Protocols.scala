package com.tomtom.dedup
import akka.routing.ConsistentHashingRouter.ConsistentHashable

case class Request(lineNumber: Int, line: String) extends ConsistentHashable {
  override def consistentHashKey: Any = line
}

case class Response(lineNumber: Int, line: String, duplicate: Boolean)
case class DontWaitForLine(number: Int)
case class EndOfFile(lineNumber: Int)

