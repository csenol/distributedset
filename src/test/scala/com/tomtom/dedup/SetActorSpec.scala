package com.tomtom.dedup.test

import akka.actor.ActorSystem
import akka.actor.Props
import akka.testkit.ImplicitSender
import akka.testkit.TestActorRef
import akka.testkit.TestKit
import org.scalatest.WordSpecLike
import scala.concurrent.duration._
import com.tomtom.dedup._

class SetActorSpec(_system: ActorSystem) extends TestKit(_system) with ImplicitSender with WordSpecLike {

  def this() = this(ActorSystem("SetActorSpec"))

  val line1 = "a"
  val line2 = "b"
  val line3 = "b"
  val setActor = system.actorOf(Props[SetActor], "setActor")
  implicit val timeOut: akka.util.Timeout = 1 seconds  

  "SetActor" should {
    "add and return response if line has NOT seen already" in {
      setActor ! Request(1, line1)
      expectMsg(Response(1, line1, duplicate = false))
    }

    "add another line and return response if line has NOT seen already" in {
      setActor ! Request(2, line2)
      expectMsg(Response(2, line2, duplicate = false))
    }

    "not add another line and return response as duplicate if line has seen already" in {
      setActor ! Request(3, line3)
      expectMsg(Response(3, line3, duplicate = true))
    }

    
  }

}
