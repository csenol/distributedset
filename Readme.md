# A Big Data word count implementation with distributed sets #

Since file was huge, we have to distrbute it for uniqness check. 
I have done it by using regular distrbuted Sets. However, actors have to be consistent with keys 
to go relevant set. Therefore I have used ConsistenHashng. Thanks to Akka they have implemented it in their routers.


To keep order of lines same, I also introduced a sequencing in `Reducer` Actor.
However, due to network errors it is possible that some packet might not arrive. So I droped "late comers"
however, it is possible to add them with little change

To scale this out, one can use remote actors or cluster. However, if 
we don't have have enough resources for scale out, we can use Bloom filters if 
we can tolerate some "missed lines" 